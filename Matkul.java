
package tugas3;


public class Matkul {
    private String kode;
    private String nama;
    private double nilaiAngka;

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public double getNilaiAngka() {
        return nilaiAngka;
    }

    public void setNilaiAngka(double nilaiAngka) {
        this.nilaiAngka = nilaiAngka;
    }

    public String getNilaiHuruf(){
        if(nilaiAngka >= 85)
            return "A";
        else if(nilaiAngka >= 80)
            return "A-";
        else if(nilaiAngka >= 75)
            return "B+";
        else if(nilaiAngka >= 70)
            return "B";
        else if(nilaiAngka >= 65)
            return "C+";
        else if(nilaiAngka >= 60)
            return "C";
        else if(nilaiAngka >= 55)
            return "D";
        else
            return "E";
    }


}