
package tugas3;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Mahasiswa mahasiswa = new Mahasiswa();
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan NIM Mahasiswa : ");
        mahasiswa.setNIM(input.nextLine());
        System.out.print("Masukkan Nama Mahasiswa : ");
        mahasiswa.setNama(input.nextLine());

        System.out.print("Masukkan Jumlah Mata Kuliah : ");
        int JumlahMatkul = input.nextInt();
        input.nextLine();

        Matkul[] matakuliah = new Matkul[JumlahMatkul];

        for (int x = 0 ; x< JumlahMatkul; x++){
            System.out.println(" Data Mata Kuliah "+(x+1));
            System.out.print("Masukkan Kode MK : ");
            matakuliah[x].setKode(input.nextLine());
            System.out.print("Masukkan Nama MK : ");
            matakuliah[x].setNama(input.nextLine());
            System.out.print("Masukkan Nilai Angka : ");
            matakuliah[x].setNilaiAngka(input.nextDouble());
            input.nextLine();


        }
        KHS khs = new KHS(mahasiswa, matakuliah);
        khs.printKHS();
    }

}