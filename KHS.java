
package tugas3;


public class KHS {

    private Mahasiswa mahasiswa;
    private Matkul[] matakuliah;


    public KHS(Mahasiswa mahasiswa, Matkul[] matakuliah) {
        this.mahasiswa = mahasiswa;
        this.matakuliah = matakuliah;
    }

    public void printKHS(){
        System.out.println(" ");
        System.out.println("============= Kartu Hasil Studi =============");
        System.out.println("NIM : "+ mahasiswa.getNIM());
        System.out.println("Nama Mahasiswa : "+ mahasiswa.getNama());
        System.out.println("+-------------------------------------------+");
        System.out.println("|   Kode   |    Nama Mata Kuliah    | Nilai |");
        System.out.println("+----------+------------------------+-------+");
        for (Matkul mk : matakuliah) {
            System.out.printf("| %-8s | %-22s | %-5s |%n", mk.getKode(), mk.getNama(), mk.getNilaiHuruf());
        }
        System.out.println("+----------+------------------------+-------+");
    }
}